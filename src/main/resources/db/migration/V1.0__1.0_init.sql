create table if not exists role (
	id bigint not null auto_increment, 
	description varchar(255), 
	name varchar(255) not null, 
	primary key (id),
	unique (name)
);

create table if not exists user (
	id bigint not null auto_increment,  
	firstname varchar(255),
	username varchar(255) not null,
	password varchar(255),
	lastname varchar(255) not null, 
	primary key (id),
	unique (username)
);

create table if not exists user_roles (
	user_id bigint not null, 
	role_id bigint not null, 
	primary key (user_id, role_id),
	constraint role_user_fk_1 foreign key (user_id) references user (id),
	constraint role_user_fk_2 foreign key (role_id) references role (id)
);

create table if not exists country (
	id bigint not null auto_increment,
	name varchar(255) not null,
	timezone varchar(255),
	primary key (id)
);

create table if not exists city (
	id bigint not null auto_increment,
	name varchar(255) not null,
	country_id bigint not null,
	description varchar(255) not null,
	primary key (id),
	constraint city_country_fk foreign  key (country_id) references country (id)
);

create table if not exists airport (
	id bigint not null,
	name varchar(255),
	city_id bigint not null,
	country_id bigint  not null,
	iata varchar(3),
	icao varchar(4),
	latitude decimal,
	longitude decimal,
	altitude bigint,
	timezone varchar(255),
	dst varchar(255),
	tz varchar(255),
	type varchar(255),
	source varchar(255),
	primary key(id),
	constraint airport_city_fk foreign key (city_id) references city (id),
	constraint airport_country_fk foreign key (country_id) references country (id)
);

create table if not exists comment (
	id bigint not null auto_increment,
	created_at datetime not null,
	updated_at datetime,
	created_by varchar(255),
	description longtext,
	city_id bigint,
	primary key(id),
	constraint comment_city_fk foreign key (city_id) references city (id)
);
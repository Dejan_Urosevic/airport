package com.demo.airport.model;

public enum DaylightSavingTime {

	E("Europe"),
	A("US/Canada"),
	S("South America"),
	O("Australia"),
	Z("New Zealand"),
	N("None"),
	U("Unknown");
	
	private String name;
	
	DaylightSavingTime(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
}

package com.demo.airport.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "user")
@NoArgsConstructor
@AllArgsConstructor
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter
	private Long id;
	
	@Column(name = "firstname")
	@Getter
	@Setter
	private String firstname;
	
	@Column(name =  "lastname")
	@Setter
	@Getter
	private String lastname;
	
	@Column(name = "username", unique = true, nullable = false)
	@Getter
	@Setter
	private String username;
	
	@Column(name = "password")
	@Getter
	@Setter
	private String password;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "user_roles", joinColumns = {
			@JoinColumn(name = "user_id")
	}, inverseJoinColumns = {
			@JoinColumn(name = "role_id")
	})
	@Getter
	@Setter
	private Set<Role> roles = new HashSet<>();

}

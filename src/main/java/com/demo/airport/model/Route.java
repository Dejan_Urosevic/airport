package com.demo.airport.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "route")
@AllArgsConstructor
@NoArgsConstructor
public class Route {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter
	private Long id;
	
	@Column(name = "airline", length = 3)
	@Getter
	@Setter
	private String airline;
	
	@Column(name = "airline_id")
	@Getter
	@Setter
	private String airlineId; 
	
	@ManyToOne
	@JoinColumn(name = "source_id")
	@Getter
	@Setter
	private Airport source;
	
	@ManyToOne
	@JoinColumn(name = "destination_id")
	@Getter
	@Setter
	private Airport destination;
	
	@Column(name = "codeshare")
	@Getter
	@Setter
	private String codeshare;
	
	@Column(name = "stops")
	@Getter
	@Setter
	private Integer stops;
	
	@Column(name = "equipment")
	@Getter
	@Setter
	private String equipment;
	
	@Column(name = "price")
	@Getter
	@Setter
	private BigDecimal price;
}

package com.demo.airport.error;

import java.io.FileNotFoundException;

import javax.validation.ValidationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javassist.NotFoundException;

@ControllerAdvice
public class ErrorHandler {

	@ExceptionHandler({ServletRequestBindingException.class, NotFoundException.class, FileNotFoundException.class, 
		MethodArgumentNotValidException.class, AccessDeniedException.class, ValidationException.class, AuthenticationException.class,
		UsernameNotFoundException.class, NullPointerException.class})
	public ResponseEntity<Error> handleError(Exception e, WebRequest request) {
		Error error = new Error();
		
		if (e instanceof ServletRequestBindingException || e instanceof AuthenticationException) {
			error.setCode("401");
			error.setMessage("Credentials not valid.");
			return new ResponseEntity<Error>(error, HttpStatus.UNAUTHORIZED);
		} else if (e instanceof NotFoundException || e instanceof FileNotFoundException || e instanceof UsernameNotFoundException) {
			error.setCode("404");
			error.setMessage(e.getMessage());
			return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
		} else if (e instanceof MethodArgumentNotValidException || e instanceof ValidationException || e instanceof NullPointerException) {
			error.setCode("400");
			error.setMessage(e.getMessage());
			return new ResponseEntity<Error>(error, HttpStatus.BAD_REQUEST);
		} else if (e instanceof AccessDeniedException) {
			error.setCode("403");
			error.setMessage(e.getMessage());
			return new ResponseEntity<Error>(error, HttpStatus.FORBIDDEN);
		} else {
			error.setCode("500");
			error.setMessage(e.getMessage());
			return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

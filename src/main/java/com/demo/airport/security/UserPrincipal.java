package com.demo.airport.security;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.demo.airport.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;

@SuppressWarnings("serial")
public class UserPrincipal implements UserDetails {

	@Getter
	private String firstname;
	
	private String username;
	
	@Getter
	private String lastname;
	
	@JsonIgnore
	private String password;
	
	private Collection<? extends GrantedAuthority> authorities;

	public UserPrincipal(String firstname, String lastname, String username, String password,
			Collection<? extends GrantedAuthority> authorities) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.password = password;
		this.authorities = authorities;
	}
	
	public static UserPrincipal create(User user) {
		List<GrantedAuthority> authorities = user.getRoles().stream().map(role -> 
				new SimpleGrantedAuthority(role.getName()))
				.collect(Collectors.toList());
		
		return new UserPrincipal(user.getFirstname(), 
				user.getLastname(),
				user.getUsername(), 
				user.getPassword(), 
				authorities);
	}
	
	@Override
	public String getUsername() {
		return username;
	}
	
	@Override
	public String getPassword() {
		return password;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}
	@Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}

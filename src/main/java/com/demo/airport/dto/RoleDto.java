package com.demo.airport.dto;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RoleDto {

	@NotBlank
	private String name;
	
	private String description;
}

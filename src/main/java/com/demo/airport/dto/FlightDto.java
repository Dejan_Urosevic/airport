package com.demo.airport.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FlightDto {

	private LinkedList<RouteDto> stops = new LinkedList();
	
	private BigDecimal totalPrice = new BigDecimal(0);
}

package com.demo.airport.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RouteDto {

	private String airline;
	
	private String airlineId;
	
	private String sourceAirport;
	
	private Long sourceAirportId;
	
	private String destinationAirport;
	
	private Long destinationAirpotId;
	
	private String codeshare;
	
	private Integer stops;
	
	private String equipment;
	
	private BigDecimal price;
}

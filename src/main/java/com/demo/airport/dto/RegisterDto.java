package com.demo.airport.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterDto {

	private String firstname;
	private String lastname;
	private String username;
	private String password;
	private List<String> roles = new ArrayList<String>();
}

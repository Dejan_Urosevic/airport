package com.demo.airport.dto;

import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CityDto {

	@NotBlank
	private String name;
	
	@NotBlank
	private String country;
	
	@NotBlank
	private String description;
	
	private List<CommentDto> comments = new ArrayList<>();
}

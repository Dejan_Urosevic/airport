package com.demo.airport.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.validation.constraints.NotBlank;

import com.demo.airport.model.DaylightSavingTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AirportDto {

	private Long id;
	
	@NotBlank
	private String name;
	
	private String city;
	
	private String country;
	
	private String iata;
	
	private String icao;
	
	private BigDecimal latitude;
	
	private BigDecimal longitude;
	
	private BigInteger altitude;
	
	private String timezone;
	
	private DaylightSavingTime dst;
	
	private String tz;
	
	private String type;
	
	private String source;
}

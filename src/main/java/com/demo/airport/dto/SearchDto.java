package com.demo.airport.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SearchDto {

	private AirportDto source;
	
	private AirportDto destination;
	
	private List<FlightDto> flights = new ArrayList<>();
}

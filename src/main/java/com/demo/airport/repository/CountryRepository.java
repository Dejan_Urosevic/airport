package com.demo.airport.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.airport.model.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {

	public Optional<Country> findByName(String name);
}

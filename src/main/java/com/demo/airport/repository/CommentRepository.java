package com.demo.airport.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.airport.model.City;
import com.demo.airport.model.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long>{

	public List<Comment> findByCityOrderByUpdatedAtDesc(City city);
	
	public List<Comment> findByCityOrderByUpdatedAtDesc(City city, Pageable pageable);
	
	public Optional<Comment> findByIdAndCreatedBy(Long id, String createdAt);
}

package com.demo.airport.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.airport.model.Airport;
import com.demo.airport.model.City;

@Repository
public interface AirportRepository extends JpaRepository<Airport, Long>{

	public Optional<Airport> findByName(String name);
	
	public List<Airport> findAllByCity(City city);
}

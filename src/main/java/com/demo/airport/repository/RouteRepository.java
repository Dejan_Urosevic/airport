package com.demo.airport.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.airport.model.Airport;
import com.demo.airport.model.Route;

@Repository
public interface RouteRepository extends JpaRepository<Route, Long>{

	public List<Route> findBySource(Airport source);
	
	public List<Route> findByDestination(Airport destination);;
}

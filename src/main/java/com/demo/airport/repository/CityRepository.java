package com.demo.airport.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.airport.model.City;

@Repository
public interface CityRepository extends JpaRepository<City, Long>{

	public Optional<City> findByName(String name);
	
	public Optional<City> findByNameAndCountry_Name(String name, String country);
}

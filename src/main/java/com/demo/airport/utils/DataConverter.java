package com.demo.airport.utils;

import java.math.BigDecimal;
import java.math.BigInteger;

public class DataConverter {

	public static BigInteger convertBigInteger(Object value) {
		BigInteger data = null;
		if (value != null) {
			if (value instanceof BigInteger)
				data = (BigInteger) value;
			else if (value instanceof String)
				data = new BigInteger(value.toString().replace("\"", ""));
			else 
				throw new ClassCastException();
		} else {
			throw new NullPointerException();
		}
		return data;
	}
	
	public static BigDecimal convertBigDecimal(Object value) {
		BigDecimal data = null;
		if (value != null) {
			if (value instanceof BigDecimal)
				data = (BigDecimal) value;
			else if (value instanceof String)
				data = new BigDecimal(value.toString().replace("\"", ""));
			else if (value instanceof BigInteger)
				data = new BigDecimal((BigInteger) value);
			else 
				throw new ClassCastException();
		} else {
			throw new NullPointerException();
		}
		return data;
	}
	
	public static String convertString(Object value) {
		String data = null;
		if (value != null) {
			if (value instanceof String) 
				data = value.toString().replace("\"", "");
			else if (value instanceof BigDecimal)
				data = ((BigDecimal) value).toString();
			else if (value instanceof BigInteger)
				data = ((BigInteger) value).toString();
		} else {
			throw new NullPointerException();
		}
		return data;
	}
	
	public static Long convertLong(Object value) {
		Long data = null;
		if (value != null) {
			if (value instanceof Long) 
				data = (Long) value;
			else if (value instanceof String)
				data = Long.parseLong(value.toString().replace("\"", ""));
		} else {
			throw new NullPointerException();
		}
		return data;
	}
	
	public static Integer convertInteger(Object value) {
		Integer data = null;
		if (value != null) {
			if (value instanceof Integer) 
				data = (Integer) value;
			else if (value instanceof String)
				data = Integer.parseInt(value.toString().replace("\"", ""));
		} else {
			throw new NullPointerException();
		}
		return data;
	}
}

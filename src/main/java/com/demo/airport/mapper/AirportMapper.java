package com.demo.airport.mapper;

import org.springframework.stereotype.Component;

import com.demo.airport.dto.AirportDto;
import com.demo.airport.model.Airport;
import com.demo.airport.repository.CityRepository;
import com.demo.airport.repository.CountryRepository;

import javassist.NotFoundException;

@Component
public class AirportMapper {

	private CityRepository cityRepository;
	private CountryRepository countryRepository;
	
	public AirportMapper(CityRepository cityRepository,
			CountryRepository countryRepository) {
		this.cityRepository = cityRepository;
		this.countryRepository = countryRepository;
	}
	
	public AirportDto airportToAirportDto(Airport airport) {
		AirportDto dto = new AirportDto();
		dto.setAltitude(airport.getAltitude());
		dto.setCity(airport.getCity().getName());
		dto.setCountry(airport.getCountry().getName());
		dto.setDst(airport.getDst());
		dto.setIata(airport.getIata());
		dto.setIcao(airport.getIcao());
		dto.setLatitude(airport.getLatitude());
		dto.setLongitude(airport.getLongitude());
		dto.setName(airport.getName());
		dto.setSource(airport.getSource());
		dto.setTimezone(airport.getTimezone());
		dto.setType(airport.getType());
		dto.setTz(airport.getTz());
		return dto;
	}
	
	public Airport airportDtoToAirport(AirportDto dto) throws NotFoundException {
		Airport airport = new Airport();
		airport.setId(dto.getId());
		airport.setAltitude(dto.getAltitude());
		airport.setCity(cityRepository.findByName(dto.getCity()).orElseThrow(() -> new NotFoundException("City not found.")));
		airport.setCountry(countryRepository.findByName(dto.getCountry()).orElseThrow(() -> new NotFoundException("Country not found.")));
		airport.setDst(dto.getDst());
		airport.setIata(dto.getIata());
		airport.setIcao(dto.getIcao());
		airport.setLatitude(dto.getLatitude());
		airport.setLongitude(dto.getLongitude());
		airport.setName(dto.getName());
		airport.setSource(dto.getName());
		airport.setTimezone(dto.getTimezone());
		airport.setType(dto.getType());
		airport.setTz(dto.getTz());
		return airport;
	}
}

package com.demo.airport.mapper;

import org.springframework.stereotype.Component;

import com.demo.airport.dto.RouteDto;
import com.demo.airport.model.Route;
import com.demo.airport.repository.AirportRepository;

import javassist.NotFoundException;

@Component
public class RouteMapper {

	private AirportRepository airportRepository;
	
	public RouteMapper(AirportRepository airportRepository) {
		this.airportRepository = airportRepository;
	}
	
	public RouteDto routeToRouteDto(Route route) {
		RouteDto dto = new RouteDto();
		dto.setAirline(route.getAirline());
		dto.setAirlineId(route.getAirlineId());
		dto.setCodeshare(route.getCodeshare());
		dto.setDestinationAirport(route.getDestination().getIata().equals("") ? route.getDestination().getIcao() : route.getDestination().getIata());
		dto.setDestinationAirpotId(route.getDestination().getId());
		dto.setEquipment(route.getEquipment());
		dto.setPrice(route.getPrice());
		dto.setSourceAirport(route.getSource().getIata().equals("") ? route.getSource().getIcao() : route.getDestination().getIata());
		dto.setSourceAirportId(route.getSource().getId());
		dto.setStops(route.getStops());
		return dto;
	}
	
	public Route routeDtoToRoute(RouteDto dto) throws NotFoundException {
		Route route = new Route();
		route.setAirline(dto.getAirline());
		route.setAirlineId(dto.getAirlineId());
		route.setCodeshare(dto.getCodeshare());
		route.setDestination(airportRepository.findById(dto.getDestinationAirpotId()).orElseThrow(() -> new NotFoundException("Destination airport not found.")));
		route.setEquipment(dto.getEquipment());
		route.setPrice(dto.getPrice());
		route.setSource(airportRepository.findById(dto.getSourceAirportId()).orElseThrow(() -> new NotFoundException("Source airport not found.")));
		route.setStops(dto.getStops());
		return route;
	}
}

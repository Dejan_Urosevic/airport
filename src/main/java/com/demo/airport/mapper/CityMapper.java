package com.demo.airport.mapper;

import org.springframework.stereotype.Component;

import com.demo.airport.dto.CityDto;
import com.demo.airport.model.City;
import com.demo.airport.repository.CountryRepository;
import com.demo.airport.service.CommentService;

@Component
public class CityMapper {
	
	private CountryRepository countryRepository;
	private CommentService commentService;
	
	public CityMapper(CountryRepository countryRepository,
			CommentService commentService) {
		this.countryRepository = countryRepository;
		this.commentService = commentService;
	}
	
	public City cityDtoToCity(CityDto dto) {
		City city = new City();
		city.setName(dto.getName());
		city.setCountry(countryRepository.findByName(dto.getCountry()).orElseThrow());
		city.setDescription(dto.getDescription());
		return city;
	}
	
	public CityDto cityToCityDto(City city, Integer limit) {
		CityDto dto = new CityDto();
		dto.setName(city.getName());
		dto.setCountry(city.getCountry().getName());
		dto.setDescription(city.getDescription());
		dto.setComments(commentService.findByCity(city, limit));
		return dto;
	}
}

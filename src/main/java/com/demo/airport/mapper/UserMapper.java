package com.demo.airport.mapper;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.demo.airport.dto.RegisterDto;
import com.demo.airport.dto.UserDto;
import com.demo.airport.model.User;
import com.demo.airport.repository.RoleRepository;
import com.demo.airport.service.RoleService;

import javassist.NotFoundException;

@Component
public class UserMapper {

	private RoleService roleService;
	private RoleRepository roleRepository;
	private PasswordEncoder encoder;
	
	public UserMapper(RoleService roleService,
			RoleRepository roleRepository,
			PasswordEncoder encoder,
			RoleMapper roleMapper) {
		this.roleService = roleService;
		this.roleRepository = roleRepository;
		this.encoder = encoder;
	}
	
	public User userDtoToUser(UserDto dto) {
		User user = new User();
		user.setFirstname(dto.getFirstname());
		user.setLastname(dto.getLastname());
		user.setUsername(dto.getUsername());
		dto.getRoles().forEach(role -> {
			try {
				user.getRoles().add(roleRepository.findByName(role).orElseThrow(() -> new NotFoundException("Role not found.")));
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		return user;
	}
	
	public UserDto userToUserDto(User user) {
		UserDto dto = new UserDto();
		dto.setFirstname(user.getFirstname());
		dto.setLastname(user.getLastname());
		dto.setUsername(user.getUsername());
		user.getRoles().forEach(role -> {
			dto.getRoles().add(role.getName());
		});
		return dto;
	}
	
	public User registerDtoToUser(RegisterDto dto) {
		User user = new User();
		user.setFirstname(dto.getFirstname());
		user.setLastname(dto.getLastname());
		user.setUsername(dto.getUsername());
		user.setPassword(encoder.encode(dto.getPassword()));
		dto.getRoles().forEach(role -> {
			try {
				user.getRoles().add(roleRepository.findByName(role).orElseThrow(() -> new NotFoundException("Role not found.")));
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		return user;
	}
}

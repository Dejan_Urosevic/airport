package com.demo.airport.mapper;

import org.springframework.stereotype.Component;

import com.demo.airport.dto.CountryDto;
import com.demo.airport.model.Country;

@Component
public class CountryMapper {

	public CountryDto countryToCountryDto(Country country) {
		CountryDto dto = new CountryDto();
		dto.setName(country.getName());
		dto.setTimezone(country.getTimezone());
		return dto;
	}
	
	public Country countryDtoToCountry(CountryDto dto) {
		Country country = new Country();
		country.setName(dto.getName());
		country.setTimezone(dto.getTimezone());
		return country;
	}
}

package com.demo.airport.mapper;

import org.springframework.stereotype.Component;

import com.demo.airport.dto.RoleDto;
import com.demo.airport.model.Role;

@Component
public class RoleMapper {

	public Role roleDtoToRole(RoleDto dto) {
		Role role = new Role();
		role.setDescription(dto.getDescription());
		role.setName(dto.getName());
		return role;
	}
	
	public RoleDto roleToRoleDto(Role role) {
		RoleDto dto = new RoleDto();
		dto.setDescription(role.getDescription());
		dto.setName(role.getName());
		return dto;
	}
}

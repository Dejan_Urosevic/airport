package com.demo.airport.mapper;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.demo.airport.dto.CommentDto;
import com.demo.airport.model.Comment;
import com.demo.airport.repository.CityRepository;

import javassist.NotFoundException;

@Component
public class CommentMapper {

	private CityRepository cityRepository;
	
	public CommentMapper(CityRepository cityRepository) {
		this.cityRepository = cityRepository;
	}
	
	public CommentDto commentToCommentDto(Comment comment) {
		CommentDto dto = new CommentDto();
		dto.setCity(comment.getCity().getName());
		dto.setCreatedAt(Date.from(comment.getCreatedAt()));
		dto.setCreatedBy(comment.getCreatedBy());
		dto.setDescription(comment.getDescription());
		dto.setId(comment.getId());
		dto.setUpdatedAt(Date.from(comment.getUpdatedAt()));
		return dto;
	}
	
	public Comment commentDtoToComment(CommentDto dto) throws NotFoundException {
		Comment comment = new Comment();
		if(dto.getId() != null)
			comment.setId(dto.getId());
		if(dto.getCity() != null)
			comment.setCity(cityRepository.findByName(dto.getCity()).orElseThrow(() -> new NotFoundException("City not found.")));
		comment.setDescription(dto.getDescription());
		return comment;
	}
}

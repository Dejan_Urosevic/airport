package com.demo.airport.auditing;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;

@SuppressWarnings("serial")
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
		value = {"createdAt", "updatedAt", "createdBy"},
        allowGetters = true
)
public abstract class DataAudit<U> implements Serializable {

	@CreatedDate
	@Column(name = "created_at", nullable = false, updatable = false)
	@Getter
	private Instant createdAt;

	@LastModifiedDate
	@Column(name = "updated_at")
	@Getter
	private Instant updatedAt;
	
	@CreatedBy
	@Column(name = "created_by", nullable = false)	
	@Getter
	private U createdBy;
}

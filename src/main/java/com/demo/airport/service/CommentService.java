package com.demo.airport.service;

import java.util.List;

import com.demo.airport.dto.CommentDto;
import com.demo.airport.model.City;

import javassist.NotFoundException;

public interface CommentService {

	public CommentDto save(CommentDto dto) throws NotFoundException;
	
	public List<CommentDto> findByCity(City city, Integer limit);
	
	public CommentDto update(CommentDto dto) throws NotFoundException;
	
	public void delete(Long id) throws NullPointerException, NotFoundException;
}

package com.demo.airport.service;

import com.demo.airport.dto.RouteDto;
import com.demo.airport.dto.SearchDto;

import javassist.NotFoundException;

public interface RouteService {

	public RouteDto save(RouteDto dto) throws NotFoundException;
	
	public void load() throws Exception;
	
}

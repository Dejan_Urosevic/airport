package com.demo.airport.service;

import java.util.List;

import com.demo.airport.dto.SearchDto;

import javassist.NotFoundException;

public interface FlightService {

	public List<SearchDto> search(String source, String destination) throws NotFoundException;
}

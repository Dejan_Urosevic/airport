package com.demo.airport.service;

import com.demo.airport.dto.RoleDto;

import javassist.NotFoundException;

public interface RoleService {

	public RoleDto save(RoleDto dto);
	
	public RoleDto findById(Long id) throws NotFoundException;
	
	public RoleDto findByName(String name) throws NotFoundException;
}

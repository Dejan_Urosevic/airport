package com.demo.airport.service;

import java.io.FileNotFoundException;

import com.demo.airport.dto.AirportDto;

import javassist.NotFoundException;

public interface AirportService {

	public AirportDto save(AirportDto airportDto) throws NotFoundException;
	
	public AirportDto findByName(String name) throws NotFoundException;
	
	public void load() throws FileNotFoundException;
}

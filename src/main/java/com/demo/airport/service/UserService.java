package com.demo.airport.service;

import com.demo.airport.dto.RegisterDto;
import com.demo.airport.dto.UserDto;

public interface UserService {

	public UserDto save(RegisterDto dto);
	
	public UserDto findByUsername(String username);
	
	public UserDto findById(Long id);
}

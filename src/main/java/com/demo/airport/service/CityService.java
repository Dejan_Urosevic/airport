package com.demo.airport.service;

import java.util.List;

import com.demo.airport.dto.CityDto;

import javassist.NotFoundException;

public interface CityService {

	public CityDto save(CityDto dto);
	
	public CityDto findById(Long id) throws NotFoundException;
	
	public CityDto findByName(String name, Integer limit) throws NotFoundException;
	
	public List<CityDto> findAll(Integer limit);
	
	public void load() throws Exception;
}

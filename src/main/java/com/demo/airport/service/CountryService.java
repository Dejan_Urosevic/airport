package com.demo.airport.service;

import java.io.FileNotFoundException;

import com.demo.airport.dto.CountryDto;

import javassist.NotFoundException;

public interface CountryService {

	public CountryDto save(CountryDto dto);
	
	public CountryDto findById(Long id) throws NotFoundException;
	
	public CountryDto findByName(String name) throws NotFoundException;
	
	public void load() throws FileNotFoundException;
}

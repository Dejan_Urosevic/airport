package com.demo.airport.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.demo.airport.dto.FlightDto;
import com.demo.airport.dto.RouteDto;
import com.demo.airport.dto.SearchDto;
import com.demo.airport.mapper.AirportMapper;
import com.demo.airport.mapper.RouteMapper;
import com.demo.airport.model.Airport;
import com.demo.airport.model.Route;
import com.demo.airport.repository.AirportRepository;
import com.demo.airport.repository.CityRepository;
import com.demo.airport.repository.RouteRepository;
import com.demo.airport.service.FlightService;

import javassist.NotFoundException;

@Service
public class FlightServiceImpl implements FlightService {

	private AirportRepository airportRepository;
	private CityRepository cityRepository;
	private RouteRepository routeRepository;
	private AirportMapper airportMapper;
	private RouteMapper routeMapper;
	
	
	public FlightServiceImpl(AirportRepository airportRepository,
			CityRepository cityRepository,
			RouteRepository routeRepository,
			AirportMapper airportMapper,
			RouteMapper routeMapper) {
		this.airportRepository = airportRepository;
		this.cityRepository = cityRepository;
		this.routeRepository = routeRepository;
		this.airportMapper = airportMapper;
		this.routeMapper = routeMapper;
	}
	
	@Override
	public List<SearchDto> search(String source, String destination) throws NotFoundException {
		List<Airport> startPoint = airportRepository.findAllByCity(cityRepository.findByName(source).orElseThrow(() -> 
			new NotFoundException("City not found.")));
		
		List<Airport> endPoint = airportRepository.findAllByCity(cityRepository.findByName(destination).orElseThrow(() -> 
			new NotFoundException("City not found.")));

		List<SearchDto> searchs = new ArrayList<>();
		
		for(int i = 0; i < startPoint.size(); i++) {
			List<Route> startRoute = routeRepository.findBySource(startPoint.get(i));
			for(int j = 0; j < endPoint.size(); j++) {
				List<LinkedList<Route>> flights = new ArrayList<>();
				SearchDto s = new SearchDto();
				s.setSource(airportMapper.airportToAirportDto(startPoint.get(i)));
				s.setDestination(airportMapper.airportToAirportDto(endPoint.get(j)));
				List<Route> endRoute = routeRepository.findByDestination(endPoint.get(j));
				startRoute.forEach(sRoute -> {	
					try {
						endRoute.stream().filter(eRoute -> eRoute.getDestination().getId() == sRoute.getDestination().getId()).findAny().orElseThrow();
						LinkedList<Route> flight = new LinkedList<>();
						flight.addFirst(sRoute);
						flights.add(flight);
						return;
					} catch (Exception e) {
						List<Route> nextStop = routeRepository.findBySource(sRoute.getDestination());
						nextStop.forEach(nRoute -> {
							LinkedList<Route> flight = new LinkedList<>();
							flight.addFirst(sRoute);
							try {
								if(endRoute.stream().filter(eRoute-> nRoute.getDestination().getId() == eRoute.getDestination().getId()).findAny().isPresent()) {
									flight.add(nRoute);
									flights.add(flight);
								} else if (endRoute.stream().filter(eRoute-> nRoute.getDestination().getId() == eRoute.getSource().getId()).findAny().isPresent()) {
									Route end = endRoute.stream().filter(eRoute-> nRoute.getDestination().getId() == eRoute.getSource().getId()).findAny().get();
									if(sRoute.getSource().getId() != nRoute.getDestination().getId()) {
										flight.add(nRoute);
										flight.addLast(end);
										flights.add(flight);
									}
								}
								return;
							} catch (Exception e2) {
								return;
							}
						});
						return;
					}
				});
				flights.forEach(flight -> {
					FlightDto flightDto = new FlightDto();
					flight.forEach(route -> {
						flightDto.getStops().add(routeMapper.routeToRouteDto(route));
						flightDto.setTotalPrice(flightDto.getTotalPrice().add(route.getPrice()));
					});
					s.getFlights().add(flightDto);
				});
				s.getFlights().sort((FlightDto f1, FlightDto f2) -> f1.getTotalPrice().compareTo(f2.getTotalPrice()));
				searchs.add(s);
			}	
		}
		return searchs;
	}
	
	
}

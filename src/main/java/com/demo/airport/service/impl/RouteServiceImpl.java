package com.demo.airport.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.demo.airport.dto.RouteDto;
import com.demo.airport.mapper.RouteMapper;
import com.demo.airport.model.Airport;
import com.demo.airport.model.Route;
import com.demo.airport.repository.AirportRepository;
import com.demo.airport.repository.RouteRepository;
import com.demo.airport.service.RouteService;
import com.demo.airport.utils.DataConverter;

import javassist.NotFoundException;

@Service
public class RouteServiceImpl implements RouteService {

	private RouteRepository routeRepository;
	private RouteMapper routeMapper;
	private AirportRepository airportRepository;
	
	public RouteServiceImpl(RouteRepository routeRepository,
			RouteMapper routeMapper,
			AirportRepository airportRepository) {
		this.routeRepository = routeRepository;
		this.routeMapper = routeMapper;
		this.airportRepository = airportRepository;
	}
	
	@Override
	@Transactional(rollbackOn = Exception.class)
	public RouteDto save(RouteDto dto) throws NotFoundException {
		return routeMapper.routeToRouteDto(routeRepository.save(routeMapper.routeDtoToRoute(dto)));
	}
	
	@Override
	public void load() throws FileNotFoundException {
		File file = ResourceUtils.getFile("classpath:dataset/routes.txt");
		List<Route> routes = new ArrayList<>();
		
		try (Stream<String> lines = Files.lines(file.toPath(), Charset.defaultCharset())){
			lines.forEach(line -> {
				List data = Arrays.asList(line.split(","));
				
				try {
					Airport source = airportRepository.findById(DataConverter.convertLong(data.get(3))).orElseThrow();
					Airport destination = airportRepository.findById(DataConverter.convertLong(data.get(5))).orElseThrow();
					Route route = new Route();
					route.setAirline(DataConverter.convertString(data.get(0))); 
					route.setAirlineId(DataConverter.convertString(data.get(1)));
					route.setSource(source);
					route.setDestination(destination);
					route.setCodeshare(DataConverter.convertString(data.get(6)));
					route.setStops(DataConverter.convertInteger(data.get(7)));
					route.setEquipment(DataConverter.convertString(data.get(8)));
					route.setPrice(DataConverter.convertBigDecimal(data.get(9)));
					routes.add(route);
				} catch (Exception e) {
					return;
				}
			});
			routeRepository.saveAll(routes);
		} catch (Exception e) {
			throw new FileNotFoundException("File routes.txt not found.");
		}
		
	}
}

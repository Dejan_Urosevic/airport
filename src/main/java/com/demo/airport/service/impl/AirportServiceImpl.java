package com.demo.airport.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.demo.airport.dto.AirportDto;
import com.demo.airport.mapper.AirportMapper;
import com.demo.airport.model.Airport;
import com.demo.airport.model.City;
import com.demo.airport.model.DaylightSavingTime;
import com.demo.airport.repository.AirportRepository;
import com.demo.airport.repository.CityRepository;
import com.demo.airport.service.AirportService;
import com.demo.airport.utils.DataConverter;

import javassist.NotFoundException;

@Service
public class AirportServiceImpl implements AirportService {

	private AirportRepository airportRepository;
	private AirportMapper airportMapper;
	private CityRepository cityRepository;
	
	public AirportServiceImpl(AirportRepository airportRepository,
			AirportMapper airportMapper,
			CityRepository cityRepository) {
		this.airportRepository = airportRepository;
		this.airportMapper = airportMapper;
		this.cityRepository = cityRepository;
	}
	
	@Override
	@Transactional(rollbackOn = Exception.class)
	public AirportDto save(AirportDto airportDto) throws NotFoundException {
		return airportMapper.airportToAirportDto(airportRepository.save(airportMapper.airportDtoToAirport(airportDto)));
	}

	@Override
	public AirportDto findByName(String name) throws NotFoundException {
		return airportMapper.airportToAirportDto(airportRepository.findByName(name).orElseThrow(() -> new NotFoundException("Airport not found.")));
	}

	@Override
	public void load() throws FileNotFoundException {
		File file = ResourceUtils.getFile("classpath:dataset/airports.txt");
		List<Airport> airports = new ArrayList<>();
		
		try (Stream<String> lines = Files.lines(file.toPath(), Charset.defaultCharset())){
			lines.forEach(line -> {
				List data = Arrays.asList(line.split(","));
				
				try {
					City city = cityRepository.findByName(DataConverter.convertString(data.get(2))).orElseThrow();

					Airport airport = new Airport();
					airport.setId(DataConverter.convertLong(data.get(0)));
					airport.setName(DataConverter.convertString(data.get(1)));
					airport.setCity(city);
					airport.setCountry(city.getCountry());
					airport.setIata(DataConverter.convertString(data.get(4)));
					airport.setIcao(DataConverter.convertString(data.get(5)));
					airport.setLatitude(DataConverter.convertBigDecimal(data.get(6)));
					airport.setLongitude(DataConverter.convertBigDecimal(data.get(7)));
					airport.setAltitude(DataConverter.convertBigInteger(data.get(8)));
					airport.setTimezone(DataConverter.convertString(data.get(9)));
					airport.setDst(DaylightSavingTime.valueOf(DataConverter.convertString(data.get(10))));
					airport.setTz(DataConverter.convertString(data.get(11)));
					airport.setType(DataConverter.convertString(data.get(12)));
					airport.setSource(DataConverter.convertString(data.get(13)));
					airports.add(airport);
				} catch (Exception e) {
					return;
				}
			});
			
			airportRepository.saveAll(airports);
		} catch (Exception e) {
			throw new FileNotFoundException("File airports.txt not found.");
		}
	}

}

package com.demo.airport.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.demo.airport.dto.CountryDto;
import com.demo.airport.mapper.CountryMapper;
import com.demo.airport.repository.CountryRepository;
import com.demo.airport.service.CountryService;
import com.demo.airport.utils.DataConverter;

import javassist.NotFoundException;

@Service
public class CountryServiceImpl implements CountryService {

	private CountryRepository countryRepository;
	private CountryMapper countryMapper;
	
	public CountryServiceImpl(CountryRepository countryRepository,
			CountryMapper countryMapper) {
		this.countryRepository = countryRepository;
		this.countryMapper = countryMapper;
	}
	
	@Override
	@Transactional(rollbackOn = Exception.class)
	public CountryDto save(CountryDto dto) {
		return countryMapper.countryToCountryDto(countryRepository.save(countryMapper.countryDtoToCountry(dto)));
	}
	
	@Override
	public CountryDto findById(Long id) throws NotFoundException {
		return countryMapper.countryToCountryDto(countryRepository.findById(id).orElseThrow(() -> new NotFoundException("Country not found.")));
	}

	@Override
	public CountryDto findByName(String name) throws NotFoundException {
		return countryMapper.countryToCountryDto(countryRepository.findByName(name).orElseThrow(() -> new NotFoundException("Country not found")));
	}

	@Override
	public void load() throws FileNotFoundException {
		File file = ResourceUtils.getFile("classpath:dataset/airports.txt");
		
		Set<String> countries = new HashSet<>();
		try (Stream<String> lines = Files.lines(file.toPath(), Charset.defaultCharset())){
			lines.forEach(line -> {
				List data = Arrays.asList(line.split(","));
				
				if(!countries.contains(DataConverter.convertString(data.get(3)))) {
					save(new CountryDto(DataConverter.convertString(data.get(3)), DataConverter.convertString(data.get(11))));
					countries.add(DataConverter.convertString(data.get(3)));
				}		
			});
			
			
		} catch (Exception e) {
			throw new FileNotFoundException("File not found.");
		}
	}

}

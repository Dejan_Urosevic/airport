package com.demo.airport.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.demo.airport.dto.CommentDto;
import com.demo.airport.mapper.CommentMapper;
import com.demo.airport.model.City;
import com.demo.airport.model.Comment;
import com.demo.airport.repository.CommentRepository;
import com.demo.airport.service.CommentService;

import javassist.NotFoundException;

@Service
public class CommentServiceImpl implements CommentService {

	private CommentRepository commentRepository;
	private CommentMapper commentMapper;
	
	public CommentServiceImpl(CommentRepository commentRepository,
			CommentMapper commentMapper) {
		this.commentRepository = commentRepository;
		this.commentMapper = commentMapper;
	}
	
	@Override
	@Transactional(rollbackOn = Exception.class)
	public CommentDto save(CommentDto dto) throws NotFoundException {
		return commentMapper.commentToCommentDto(commentRepository.save(commentMapper.commentDtoToComment(dto)));
	}

	@Override
	public List<CommentDto> findByCity(City city, Integer limit) {
		List<Comment> comments = null;
		if(limit != null) 
			comments = commentRepository.findByCityOrderByUpdatedAtDesc(city, PageRequest.of(0, limit));
		else	
			comments = commentRepository.findByCityOrderByUpdatedAtDesc(city);
		
		List<CommentDto> dtos = new ArrayList<>();
		comments.forEach(comment -> {
			dtos.add(commentMapper.commentToCommentDto(comment));
		});
		return dtos;
	}

	@Override
	@Transactional(rollbackOn = Exception.class)
	public CommentDto update(CommentDto dto) throws NotFoundException {
		if(dto.getId() == null) {
			throw new NullPointerException("Comment id is null.");
		}
		String authorizatedUser = SecurityContextHolder.getContext().getAuthentication().getName();
		Comment comment = commentRepository.findByIdAndCreatedBy(dto.getId(), authorizatedUser).orElseThrow(() -> new NotFoundException("Comment not found."));
		comment.setDescription(dto.getDescription());
		return commentMapper.commentToCommentDto(commentRepository.save(comment));
	}

	@Override
	public void delete(Long id) throws NullPointerException, NotFoundException {
		if(id == null) {
			throw new NullPointerException("Comment id is null.");
		}
		String authorizatedUser = SecurityContextHolder.getContext().getAuthentication().getName();
		Comment comment = commentRepository.findByIdAndCreatedBy(id, authorizatedUser).orElseThrow(() -> new NotFoundException("Comment not found."));
		commentRepository.delete(comment);
	}

	
}

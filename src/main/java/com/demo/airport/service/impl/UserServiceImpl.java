package com.demo.airport.service.impl;

import javax.transaction.Transactional;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.demo.airport.dto.RegisterDto;
import com.demo.airport.dto.UserDto;
import com.demo.airport.mapper.UserMapper;
import com.demo.airport.model.User;
import com.demo.airport.repository.UserRepository;
import com.demo.airport.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	private UserMapper userMapper;
	
	public UserServiceImpl(UserRepository userRepository,
			UserMapper userMapper) {
		this.userRepository = userRepository;
		this.userMapper = userMapper;
	}
	
	@Override
	@Transactional(rollbackOn = Exception.class)
	public UserDto save(RegisterDto dto) {
		User user = userMapper.registerDtoToUser(dto);
		return userMapper.userToUserDto(userRepository.save(user));
	}

	@Override
	public UserDto findByUsername(String username) {
		return userMapper.userToUserDto(userRepository.findByUsername(username).orElseThrow(() -> 
				new UsernameNotFoundException("User not found."))
			);
	}

	@Override
	public UserDto findById(Long id) {
		return userMapper.userToUserDto(userRepository.findById(id).orElseThrow(() -> 
				new UsernameNotFoundException("User not found."))
			);
	}
}

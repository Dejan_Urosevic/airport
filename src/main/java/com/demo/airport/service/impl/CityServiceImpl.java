package com.demo.airport.service.impl;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.demo.airport.dto.CityDto;
import com.demo.airport.mapper.CityMapper;
import com.demo.airport.repository.CityRepository;
import com.demo.airport.service.CityService;
import com.demo.airport.utils.DataConverter;

import javassist.NotFoundException;

@Service
public class CityServiceImpl implements CityService {

	private CityRepository cityRepository;
	private CityMapper cityMapper;
	
	public CityServiceImpl(CityRepository cityRepository,
			CityMapper cityMapper) {
		this.cityRepository = cityRepository;
		this.cityMapper = cityMapper;
	}
	
	@Override
	@Transactional(rollbackOn = Exception.class)
	public CityDto save(CityDto dto) {
		return cityMapper.cityToCityDto(cityRepository.save(cityMapper.cityDtoToCity(dto)), null);
	}

	@Override
	public CityDto findById(Long id) throws NotFoundException {
		return cityMapper.cityToCityDto(cityRepository.findById(id).orElseThrow(() -> new NotFoundException("City not found.")), null);
	}

	@Override
	public CityDto findByName(String name, Integer limit) throws NotFoundException {
		return cityMapper.cityToCityDto(cityRepository.findByName(name).orElseThrow(() -> new NotFoundException("City not found.")), limit);
	}

	@Override
	public List<CityDto> findAll(Integer limit) {
		List<CityDto> cities = new ArrayList<>();
		cityRepository.findAll().iterator().forEachRemaining(city -> {
			cities.add(cityMapper.cityToCityDto(city, limit));
		});
		return cities;
	}

	@Override
	public void load() throws Exception {
		File file = ResourceUtils.getFile("classpath:dataset/airports.txt");
		
		Set<String> cities = new HashSet<>();
		
		try (Stream<String> lines = Files.lines(file.toPath(), Charset.defaultCharset())){
			lines.forEach(line -> {
				List data = Arrays.asList(line.split(","));
				
				if(!cities.contains(DataConverter.convertString(data.get(2)))) {
					CityDto city = new CityDto();
					city.setCountry(DataConverter.convertString(data.get(3)));
					city.setDescription("City " + DataConverter.convertString(data.get(2)) + " country " + DataConverter.convertString(data.get(3)));
					city.setName(DataConverter.convertString(data.get(2)));
					save(city);
					cities.add(DataConverter.convertString(data.get(2)));
				}		
			});
			
		} catch (Exception e) {
			throw new Exception();
		}
	}

}

package com.demo.airport.service.impl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.demo.airport.dto.RoleDto;
import com.demo.airport.mapper.RoleMapper;
import com.demo.airport.repository.RoleRepository;
import com.demo.airport.service.RoleService;

import javassist.NotFoundException;

@Service
public class RoleServiceImpl implements RoleService {

	private RoleRepository roleRepository;
	private RoleMapper roleMapper;
	
	public RoleServiceImpl(RoleRepository roleRepository,
			RoleMapper roleMapper) {
		this.roleRepository = roleRepository;
		this.roleMapper = roleMapper;
	}
	
	@Override
	@Transactional(rollbackOn = Exception.class)
	public RoleDto save(RoleDto dto) {
		return roleMapper.roleToRoleDto(roleRepository.save(roleMapper.roleDtoToRole(dto)));
	}

	@Override
	public RoleDto findById(Long id) throws NotFoundException {
		return roleMapper.roleToRoleDto(roleRepository.findById(id).orElseThrow(() -> new NotFoundException("Role not found.")));

	}

	@Override
	public RoleDto findByName(String name) throws NotFoundException {
		return roleMapper.roleToRoleDto(roleRepository.findByName(name).orElseThrow(() -> new NotFoundException("Role not found.")));
	}

}

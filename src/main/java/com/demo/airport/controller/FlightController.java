package com.demo.airport.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.airport.dto.SearchDto;
import com.demo.airport.service.FlightService;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = "/api/flight")
public class FlightController {

	private FlightService flightService;
	
	public FlightController(FlightService flightService) {
		this.flightService = flightService;
	}
	
	@GetMapping(value = "/search")
	@Secured("ROLE_USER")
	public ResponseEntity<?> search(@RequestParam(required = true) String source, @RequestParam(required = true) String destination) throws NotFoundException {
		return new ResponseEntity<List<SearchDto>>(flightService.search(source, destination), HttpStatus.OK);
	}
}

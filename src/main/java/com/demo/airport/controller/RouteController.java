package com.demo.airport.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.airport.dto.FlightDto;
import com.demo.airport.dto.SearchDto;
import com.demo.airport.service.RouteService;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = "/api/route")
public class RouteController {

	private RouteService routeService;
	
	public RouteController(RouteService routeService) {
		this.routeService = routeService;
	}
	
	@PostMapping(value = "/load")
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> load() throws Exception {
		routeService.load();
		return new ResponseEntity<>(HttpStatus.OK);
	}
}

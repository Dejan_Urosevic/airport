package com.demo.airport.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.airport.dto.UserDto;
import com.demo.airport.mapper.UserMapper;
import com.demo.airport.service.UserService;

@RestController
@RequestMapping(value = "/api/user")
public class UserController {

	private UserService userService;
	public UserController(UserService userService,
			UserMapper userMapper) {
		this.userService = userService;
	}
	
	@GetMapping
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> getAll(){
		return new ResponseEntity<UserDto>(userService.findById(1l), HttpStatus.OK);
	}
}

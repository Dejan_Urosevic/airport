package com.demo.airport.controller;

import java.io.FileNotFoundException;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.airport.dto.CountryDto;
import com.demo.airport.service.CountryService;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = "/api/country")
public class CountryController {

	private CountryService countryService;
	
	public CountryController(CountryService countryService) {
		this.countryService = countryService;
	}
	
	@PostMapping
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> save(@Valid @RequestBody CountryDto countryDto) {
		return new ResponseEntity<CountryDto>(countryService.save(countryDto),HttpStatus.CREATED);
	}
	
	@PostMapping(value = "/load")
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> load() throws FileNotFoundException {
		countryService.load();
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping(value = "/name")
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	public ResponseEntity<?> getByName(@RequestParam String name) throws NotFoundException {
		return new ResponseEntity<CountryDto>(countryService.findByName(name), HttpStatus.OK);
	}
}

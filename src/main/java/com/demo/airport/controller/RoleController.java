package com.demo.airport.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.airport.dto.RoleDto;
import com.demo.airport.service.RoleService;

@RestController
@RequestMapping(value = "/api/role")
public class RoleController {

	private RoleService roleService;
	
	public RoleController(RoleService roleService) {
		this.roleService = roleService;
	}
	
	@PostMapping
	public ResponseEntity<?> save(@Valid @RequestBody RoleDto roleDto) {
		return new ResponseEntity<RoleDto>(roleService.save(roleDto), HttpStatus.CREATED);
	}
}

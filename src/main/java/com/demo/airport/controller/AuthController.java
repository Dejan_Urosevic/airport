package com.demo.airport.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.airport.dto.LoginDto;
import com.demo.airport.dto.RegisterDto;
import com.demo.airport.dto.UserDto;
import com.demo.airport.security.JwtTokenProvider;
import com.demo.airport.service.RoleService;
import com.demo.airport.service.UserService;

@RestController
@RequestMapping(value = "/api/auth")
public class AuthController {
	
	AuthenticationManager authenticationManager;
	JwtTokenProvider tokenProvider;
	PasswordEncoder encoder;
	RoleService roleService;
	UserService userService;
	
	public AuthController(AuthenticationManager authenticationManager,
			JwtTokenProvider provider,
			PasswordEncoder encoder,
			RoleService roleService,
			UserService userService) {
		this.authenticationManager = authenticationManager;
		this.tokenProvider = provider;
		this.encoder = encoder;
		this.roleService = roleService;
		this.userService = userService;
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> login(@Valid @RequestBody LoginDto login) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(login.getUsername(), 
						login.getPassword())
				);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = tokenProvider.generateToken(authentication);
		return ResponseEntity.ok(jwt);
	}
	
	@PostMapping("/register")
	public ResponseEntity<?> register(@RequestBody RegisterDto register){
		
		UserDto user = userService.save(register);
		
		return new ResponseEntity<UserDto>(user, HttpStatus.CREATED);
	}

}

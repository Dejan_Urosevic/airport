package com.demo.airport.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.airport.dto.CommentDto;
import com.demo.airport.model.City;
import com.demo.airport.repository.CityRepository;
import com.demo.airport.service.CommentService;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = "/api/comment")
public class CommentController {
	
	private CommentService commentService;
	private CityRepository cityRepository;
	
	public CommentController(CommentService commentService,
			CityRepository cityRepository) {
		this.commentService = commentService;
		this.cityRepository = cityRepository;
	}

	@PostMapping
	@Secured("ROLE_USER")
	public ResponseEntity<?> save(@Valid @RequestBody CommentDto dto) throws NotFoundException {
		return new ResponseEntity<CommentDto>(commentService.save(dto), HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/city")
	@Secured("ROLE_USER")
	public ResponseEntity<?> findByCity(@RequestParam String cityName) throws NotFoundException {
		City city = cityRepository.findByName(cityName).orElseThrow(() -> new NotFoundException("City not found."));
		return new ResponseEntity<List<CommentDto>>(commentService.findByCity(city, null), HttpStatus.OK);
	}
	
	@PutMapping
	@Secured("ROLE_USER")
	public ResponseEntity<?> update(@Valid @RequestBody CommentDto dto) throws Exception {
		return new ResponseEntity<CommentDto>(commentService.update(dto), HttpStatus.ACCEPTED);
	}
	
	@DeleteMapping
	@Secured("ROLE_USER")
	public ResponseEntity<?> delete(@RequestParam Long id) throws Exception {
		commentService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}

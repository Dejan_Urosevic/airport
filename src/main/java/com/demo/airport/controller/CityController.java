package com.demo.airport.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.airport.dto.CityDto;
import com.demo.airport.service.CityService;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = "/api/city")
public class CityController {

	private CityService cityService;
	
	public CityController(CityService cityService) {
		this.cityService = cityService;
	}
	
	@PostMapping
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> save(@Valid @RequestBody CityDto dto) {
		return new ResponseEntity<CityDto>(cityService.save(dto), HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/name")
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	public ResponseEntity<?> getByName(@RequestParam String name, @RequestParam(required = false) Integer limit) throws NotFoundException {
		return new ResponseEntity<CityDto>(cityService.findByName(name, limit), HttpStatus.OK);
	}
	
	@GetMapping(value = "/all")
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	public ResponseEntity<?> getAll(@RequestParam Integer limit) {
		return new ResponseEntity<List<CityDto>>(cityService.findAll(limit), HttpStatus.OK);
	}
	
	@PostMapping(value = "/load")
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> load() throws Exception {
		cityService.load();
		return new ResponseEntity<>(HttpStatus.OK);
	}
}

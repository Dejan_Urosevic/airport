package com.demo.airport.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.airport.dto.AirportDto;
import com.demo.airport.service.AirportService;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = "/api/airport")
public class AirportController {

	private AirportService airportService;
	
	public AirportController(AirportService airportService) {
		this.airportService = airportService;
	}
	
	@PostMapping
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> save(@Valid @RequestBody AirportDto dto) throws NotFoundException {
		return new ResponseEntity<AirportDto>(airportService.save(dto), HttpStatus.CREATED);
	}
	
	@PostMapping(value = "/load")
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> load() throws Exception {
		airportService.load();
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
}

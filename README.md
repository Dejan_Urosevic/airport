#Steps

*  Register roles
*  Register users
*  Login with admin or user
*  Copy token to Authorization header with Bearer prefix
*  Add country
*  Add city
*  Add route
*  Search

Registration and login routes don't require authentication, any other route requires authentication. 

File airport.postman_collection.json contains all necessary requests for the application.

